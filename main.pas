uses  crt,graph;
type
	Point = array[0..1] of REAL;

	LineType = record
		A, B, C: REAL;
	end;

	Circle = record
		Center: Point;
		Radius: REAL;
	end;

	Answer = record
		line: LineType;
		diffeCnt: integer;
		points: array[0..1] of Point;
	end;
//==============================================================================

label mark, mark1;
var
	circ: Circle;
	points: array of Point;
	i: integer;
	ans: Answer;
	answerQuestion, fileName: string;
	timeDelay: integer;
//==============================================================================

PROCEDURE fLineIn_2Point(a, b: Point; VAR line1: LineType);
begin
	line1.A := a[1] - b[1];
	line1.B := b[0] - a[0];
	line1.C := a[0]*(b[1] - a[1]) + a[1]*(a[0] - b[0]);
end;
//==============================================================================

PROCEDURE graphInit();
var a,b: integer;
begin
	a := detect;
	b := 0;
	Initgraph(a,b,'  ');
end;
//==============================================================================

PROCEDURE initValueInTerminal();
var len:integer;
begin
	write('count point = ');
	readln(len);
	SetLength(points, len);
	writeln('value point');
	for i := 0 to len-1 do
	begin
		write('M_', i, ' x y = ');
		read(points[i][0]);
		readln(points[i][1]);
	end;
	write('circle radius = ');
	readln(circ.radius);
	write('circle point center x y = ');
	read(circ.center[0]);
	readln(circ.center[1]);
end;
//==============================================================================

PROCEDURE writeTerminal();
begin
	write('count point = ');
	writeln(length(points));
	writeln('value point');
	for i := 0 to length(points)-1 do
	begin
		write('M_', i, ' x y = ');
		write(points[i][0]:4:4);
		write(' ');
		writeln(points[i][1]:4:4);
	end;
	write('circle radius = ');
	writeln(circ.radius:4:4);
	write('circle point center x y = ');
	writeLn(circ.center[0]:4:4, ' ', circ.center[1]:4:4);

	writeLn();
	writeLn('Answer');
	writeLn('line: ', ans.line.A:4:4, 'x + ', ans.line.B:4:4, 'y + ', ans.line.C:4:4, ' = 0');
	writeln('points:');
	writeln(ans.points[0][0]:4:4, ' ', ans.points[0][1]:4:4);
	writeln(ans.points[1][0]:4:4, ' ', ans.points[1][1]:4:4);
end;

PROCEDURE writeFile(fileName: string);
var
	f: text;
begin
	assign(f, fileName);
	rewrite(f);
	writeln(f, length(points));
	for i := 0 to length(points)-1 do
	begin
		write(f, points[i][0]:4:4, ' ');
		writeLn(f, points[i][1]:4:4);
	end;
	writeln(f, circ.radius:4:4);
	write(f, circ.center[0]:4:4, ' ');
	writeln(f, circ.center[1]:4:4);

	writeLn(f);
	writeLn(f, 'Answer');
	writeLn(f, 'line: ', ans.line.A:4:4, 'x + ', ans.line.B:4:4, 'y + ', ans.line.C:4:4, ' = 0');
	writeln(f, 'points:');
	writeln(f, ans.points[0][0]:4:4, ' ', ans.points[0][1]:4:4);
	writeln(f, ans.points[1][0]:4:4, ' ', ans.points[1][1]:4:4);
	close(f);
end;
//==============================================================================
PROCEDURE initValueInFile(fileName: string);
var
	f: text;
	len:integer;
begin
	assign(f, fileName);
	reset(f);
	read(f, len);
	SetLength(points, len);
	for i := 0 to len - 1 do
	begin
		read(f, points[i][0]);
		read(f, points[i][1]);
	end;
	readln(f, circ.radius);
	read(f, circ.center[0]);
	readln(f, circ.center[1]);
	close(f);
end;
//==============================================================================

PROCEDURE draw();
var
	minX, maxX: real;
	minY, maxY: real;
	kX, kY: real;
	line0: LineType;
	i, j, k: integer;
	cntBot, cntTop: integer;
	tmpReal: real;
	pointsInsedeCirc: array of Point;
begin
	clearDevice;
	//declare region visible
	maxX := circ.center[0] + circ.radius;
	minX := circ.center[0] - circ.radius;
	maxY := circ.center[1] + circ.radius;
	minY := circ.center[1] - circ.radius;
	for i := 0 to length(points)-1 do
	begin
		if points[i][0] > maxX then
			maxX := points[i][0];
		if points[i][0] < minX then
			minX := points[i][0];
		if points[i][1] > maxY then
			maxY := points[i][1];
		if points[i][1] < minY then
			minY := points[i][1];
	end;

	// shift to begin coordinat
	circ.center[0] := circ.center[0] - minX;
	circ.center[1] := circ.center[1] - minY;
	for i := 0 to length(points)-1 do
	begin
		points[i][0] := points[i][0] - minX;
		points[i][1] := points[i][1] - minY;
	end;

	// coefficients of the proportion of the mathematical model per pixel computer
	kX := getMaxX/(maxX - minX);
	kY := getMaxY/(maxY - minY);

	// draw ellipse
	setColor(15);
	ellipse(round(kX*circ.center[0]),
	        round(getMaxY - kY*circ.center[1]),
	        0, 360,
	        round(kX*circ.radius),
	        round(kY*circ.radius));

	// draw points
	setFillStyle(1, 15);
	for i := 0 to length(points)-1 do
	begin
		fillEllipse(round(kX*points[i][0]),
		            round(getMaxY - kY*points[i][1]),
		            5, 5);
	end;

	//select separately array points inside circle
	SetLength(pointsInsedeCirc, length(points));
	j := 0;
	for i := 0 to length(points) do
	begin
		if (sqr(points[i][0] - circ.center[0]) +
		   sqr(points[i][1] - circ.center[1])
		   <= sqr(circ.radius)) then
		begin
			pointsInsedeCirc[j] := points[i];
			j := j + 1;
		end;
	end;
	SetLength(pointsInsedeCirc, j);

	ans.diffeCnt := length(points)-2;
	ans.line.A := 0;
	ans.line.B := 0;
	ans.line.C := 0;
	for i := 0 to length(points)-1 do
	begin
		for j := i + 1 to length(points)-1 do
		begin
			// draw ellipse
			setColor(15);
			ellipse(round(kX*circ.center[0]),
			        round(getMaxY - kY*circ.center[1]),
			        0, 360,
			        round(kX*circ.radius),
			        round(kY*circ.radius));

			// draw points
			setFillStyle(1, 15);
			for k := 0 to length(points)-1 do
			begin
				fillEllipse(round(kX*points[k][0]),
				            round(getMaxY - kY*points[k][1]),
				            5, 5);
			end;

			// active points
			setColor(red);
			setFillStyle(1, red);
			fillEllipse(round(kX*points[i][0]),
			            round(getMaxY - kY*points[i][1]),
			            5, 5);
			setFillStyle(1, red);
			fillEllipse(round(kX*points[j][0]),
			            round(getMaxY - kY*points[j][1]),
			            5, 5);

			fLineIn_2Point(points[i], points[j], line0);

			//A*x + B*y + C = 0
			//-A/B*x - C/B = y
			//B = 0 x = -C/A
			//-B/A*y - C/A = x
			//A = 0 y = -C/B
			// draw line in two suspicious points
			setColor(red);
			if line0.B = 0 then
				line( round(-kX*line0.C/line0.A),
				      0,
				      round(-kX*line0.C/line0.A),
				      getMaxY)
			else
				line( 0,
				      round(getMaxY - (-kY*line0.C/line0.B)),
				      getMaxX,
				      round(getMaxY - kY*(-line0.A/line0.B*(maxX - minX) - line0.C/line0.B)));

			cntBot := 0;
			cntTop := 0;
			for k := 0 to length(pointsInsedeCirc)-1 do
			begin
				// points in witch go line
				if ((points[i][0] = pointsInsedeCirc[k][0]) and
					(points[i][1] = pointsInsedeCirc[k][1])) or
					((points[j][0] = pointsInsedeCirc[k][0]) and
					(points[j][1] = pointsInsedeCirc[k][1])) then
					continue;

				// draw red point over white
				setFillStyle(1, red);
				setColor(red);
				fillEllipse(round(kX*pointsInsedeCirc[k][0]),
				            round(getMaxY - kY*pointsInsedeCirc[k][1]),
				            5, 5);

				tmpReal := line0.A*pointsInsedeCirc[k][0] + line0.B*pointsInsedeCirc[k][1] + line0.C;
				if tmpReal < 0 then
					cntBot := cntBot + 1
				else if tmpReal > 0 then
					cntTop := cntTop + 1;

				delay(timeDelay);

				// draw white point over red
				setFillStyle(1, 15);
				setColor(15);
				fillEllipse(round(kX*pointsInsedeCirc[k][0]),
				            round(getMaxY - kY*pointsInsedeCirc[k][1]),
				            5, 5);
			end;

			if abs(cntTop - cntBot) < ans.diffeCnt then
			begin
				ans.diffeCnt := abs(cntTop - cntBot);
				ans.line := line0;
				ans.points[0] := points[i];
				ans.points[1] := points[j];
			end;

			// writeln('cntBot = ', cntBot);
			// writeln('cntTop = ', cntTop);
			// readln();
			clearDevice;
		end;
	end;

	// the end result
	// draw ellipse
	clearDevice;
	setColor(15);
	ellipse(round(kX*circ.center[0]),
	        round(getMaxY - kY*circ.center[1]),
	        0, 360,
	        round(kX*circ.radius),
	        round(kY*circ.radius));

	// draw points
	setFillStyle(1, 15);
	for k := 0 to length(points)-1 do
	begin
		fillEllipse(round(kX*points[k][0]),
		            round(getMaxY - kY*points[k][1]),
		            5, 5);
	end;

	// active points
	setColor(red);
	setFillStyle(1, red);
	fillEllipse(round(kX*ans.points[0][0]),
	            round(getMaxY - kY*ans.points[0][1]),
	            5, 5);
	setFillStyle(1, red);
	fillEllipse(round(kX*ans.points[1][0]),
	            round(getMaxY - kY*ans.points[1][1]),
	            5, 5);

	//A*x + B*y + C = 0
	//-A/B*x - C/B = y
	//B = 0 x = -C/A
	//-B/A*y - C/A = x
	//A = 0 y = -C/B
	// draw line in two points
	setColor(red);
	if ans.line.B = 0 then
		line( round(-kX*ans.line.C/ans.line.A),
		      0,
		      round(-kX*ans.line.C/ans.line.A),
		      getMaxY)
	else
		line( 0,
		      round(getMaxY - (-kY*ans.line.C/ans.line.B)),
		      getMaxX,
		      round(getMaxY - kY*(-ans.line.A/ans.line.B*(maxX - minX) - ans.line.C/ans.line.B)));
end;
//==============================================================================

begin
	timeDelay := 0;
	graphInit();
	initValueInFile('input.txt');
	draw();
	writeTerminal();
	readln();
	while true do
	begin
		mark1:
		writeln();
		writeLn('Reade data');
		writeLn('1. File');
		writeLn('2. Keyboard');
		write('Answer: ');
		readLn(answerQuestion);
		case answerQuestion of
			'1':
				begin
					write('File name: ');
					readLn(fileName);
					initValueInFile(fileName);
				end;
			'2': initValueInTerminal()
			else
				begin
					writeLn('Error write data. Try again.');
					goto mark1;
				end;
		end;

		draw();

		mark:
		writeln();
		writeLn('1. Write data in file');
		writeLn('2. Write data in terminal');
		writeLn('3. Continue');
		writeLn('4. Exit');
		write('Answer: ');
		readLn(answerQuestion);
		case answerQuestion of
			'1':
				begin
					write('File name: ');
					readLn(fileName);
					writeFile(fileName);
					goto mark;
				end;
			'2': writeTerminal();
			'3': continue;
			'4': break
			else
				begin
					writeLn('Error write data. Try again.');
					goto mark;
				end;
		end;
	end;
end.